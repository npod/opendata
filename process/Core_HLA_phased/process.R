library(data.table)
library(dplyr)

# HLA data needs to be cleaned before phasing -----------------------------------------------------#
HLA <- fread("HiResHLA_2019-12-24.tsv", na.strings = "")
keep <- c("nPOD CaseID",
          "A_1", "A_2",
          "B_1", "B_2",
          "C_1", "C_2",
          "DRB1_1", "DRB1_2",
          "DQA1_1", "DQA1_2",
          "DQB1_1", "DQB1_2",
          "DPA1_1", "DPA1_2",
          "DPB1_1", "DPB1_2")
HLA <- HLA[, keep, with = F]
setnames(HLA, c("ID", gsub("_", ".", keep[-1], fixed = T)))

HLA[, ID := as.numeric(ID)]
# Remove IDs which include HPAP, cell lines, and where there is no data
HLA <- HLA[!is.na(ID)]
nodata <- apply(HLA[, -c("ID")], 1, function(x) all(is.na(x)))
HLA <- HLA[!nodata, ]

# Half of the data uses "XX:XX" nomenclature format while half does not
# For all columns except ID, fix to use current nomenclature
colonFormat <- function(col) sapply(col, function(x) if(grepl("^[0-9]{4}$", x)) sub("(?<=.{2})", ":", x, perl = TRUE) else x)
HLAcols <- setdiff(names(HLA), c("ID"))
HLA[, (HLAcols) := lapply(.SD, colonFormat), .SDcols = HLAcols]

# Remaining entries that don't have an entry in format "##:##"
check_clean <- list()
for(col in HLAcols) check_clean[[col]] <- grep("^[0-9]{2}:[0-9]{2}$", HLA[[col]], invert = T, val = T)
# View hits within each column
lapply(check_clean, function(x) x[!is.na(x)])
# File contains a number of inconsistent entries, e.g.
# "Second s" "16XX" "11:1" "11:001" "##:##/#" (ambiguous)
# For corrections, the database at https://www.ebi.ac.uk/ipd/imgt/hla/allele.html was consulted;
# most two digit entries (i.e. A*24) and ambiguous entries are ignored and left as is.
corrections <- list(
A.2 = c("33:3" = "33:03"),
DRB1.1 = c("Second s" = NA_character_),
DRB1.2 = c("-" = NA_character_,
           "16XX" = "16:XX",
           "11:1" = "11:01", # could be 11:10, but assignment to 11:01 is much more likely
           "11:001" = "11:01"), # DRB1*11:001 does not exist
DQA1.2 = c("-" = NA_character_),
DPB1.1 = c("08" = "08:01", # https://www.ebi.ac.uk/cgi-bin/ipd/imgt/hla/get_allele.cgi?DPB1*08:01 (only option for search DPB1*08)
          "15" = "15:01"),
DPB1.2 = c("601" = "06:01", # could be 06:01 or 601:01, but former much more likely than https://www.ebi.ac.uk/cgi-bin/ipd/imgt/hla/get_allele.cgi?DPB1*601:01
           "501" = "05:01", # could be 05:01 or 501:01, but former much more likely than https://www.ebi.ac.uk/cgi-bin/ipd/imgt/hla/get_allele.cgi?DPB1*501:01
           "402" = "04:02", # could be 04:02 or 402:01, but former much more likely than https://www.ebi.ac.uk/cgi-bin/ipd/imgt/hla/get_allele.cgi?DPB1*402:01
           "35" = "35:01", # https://www.ebi.ac.uk/cgi-bin/ipd/imgt/hla/get_allele.cgi?DPB1*35:01 (only option for search DPB1*35)
           "44" = "44:01") # https://www.ebi.ac.uk/cgi-bin/ipd/imgt/hla/get_allele.cgi?DPB1*44:01 (only option for search DPB1*44)
)
for(col in names(corrections)) HLA[[col]] <- recode(HLA[[col]], !!!corrections[[col]])

# Export intermediate
write.table(HLA, file = "HLAclean_ref.tsv", sep = "\t", row.names = F, quote = F)

# Phasing --------------------------------------------------------------------------------------------------------- #

source("phaseHLA_fun.R")

# -- Reference data ----------------------------------------------------------------------------------------------- #

# Data from https://frequency.nmdp.org/jsp/haploTypeFrequencies.jsp
# Download of data requires logging in from https://frequency.nmdp.org/

# Note that there is a web tool to provided at https://www.haplostats.org/haplostats?execution=e3s1
# that uses this reference data for phasing, but only a single input can be provided at a time.
# See https://bioinformatics.bethematchclinical.org/workarea/downloadasset.aspx?id=6339.
H.freq <- fread("A~C~B~DRB1~DQB1.tsv")

dem <- fread("Demographics_2019-12-24.tsv")
dem$`nPOD CaseID` <- as.numeric(dem$`nPOD CaseID`)
# Need race data from demographics table
HLA <- merge(HLA, dem[, .(`nPOD CaseID`, Race)], by.x = "ID", by.y = "nPOD CaseID")

loci <- c("A", "B", "C", "DRB1", "DQB1")
coloci <- setNames(unlist(lapply(loci, paste0, c(".1", ".2"))), rep(loci, each = 2))
# maps the race labels from nPOD to ones from nmdp.org
race.map <- list("Caucasian" = "CAU_freq", "African Am" = "AFA_freq", "Hispanic/Latino" = "HIS_freq",
                  "American Indian/Alaska Native" = "NAM_freq", "Asian" = "API_freq",
                  "Multiracial" = c("AFA_freq", "API_freq", "CAU_freq", "HIS_freq", "NAM_freq"))


# Minor cleaning and keying of reference table; convert entries in format ""A*11:01g" -> "11:01"
H.freq[, (loci) := lapply(.SD, function(x) gsub("[^0-9:]", "", sapply(strsplit(x, "*", fixed = T), `[[`, 2))), .SDcols = loci]
setkeyv(H.freq, loci)


# PHASING 1 (A~B~C~DRB1~DQB1 using NMDP reference) ----------------------------------------------------------------------------#

# B* and C* has the most missing data:
HLA[, lapply(.SD, function(x) sum(is.na(x))), .SDcols = coloci]
# A.1 A.2 B.1 B.2 C.1 C.2 DRB1.1 DRB1.2 DQB1.1 DQB1.2
# 9   9   238 238 340 340      1      1      1      1

# Get case IDs without any missing/low-res/ambiguous haplotyping info for phasing with c("A, "B", "C", "DRB1", "DQB1")
L5 <- HLA[HLA[, Reduce(`&`, lapply(.SD, function(x) grepl("^[0-9]{2}:[0-9]{2}", x))), .SDcols = coloci], c("ID", "Race", coloci), with = F ]
# This yields a subset of n = 133, for which we use the default phase function
L5.input <- melt(L5, measure.vars = patterns(paste0("^", loci)), value.name = loci)
L5.results <- lapply(split(L5.input, by = "ID"), phase, loci = loci)
L5.phased <- rbindlist(lapply(L5.results, function(x) as.data.table(x[1:5])))
setnames(L5.phased, c("ID", "Race", paste0(rep(loci, 2), rep(c(".1", ".2"), each = 5)), "max.pair.freq"))

# For 15, the output haplotype combination isn't observed in the reference;
# this could be because of haplotype rarity (esp with restriction of observervations by race), inaccuration race information,
# but the prediction might be less inaccurate for these;
# for clarity we want to see if the diplotype is observed if we don't restrict observations by race
hap0 <- which(L5.phased$max.pair.freq == 0)
L5x <- L5[hap0, ]
L5x <- L5x[, Race := "Multiracial"] # to use all available reference observations
L5.inputx <- melt(L5x, measure.vars = patterns(paste0("^", loci)), value.name = loci)
L5.resultsx <- lapply(split(L5.inputx, by = "ID"), phase, loci = loci)
L5.phasedx <- rbindlist(lapply(L5.resultsx, function(x) as.data.table(x[1:5])))
setnames(L5.phasedx, c("ID", "Race", paste0(rep(loci, 2), rep(c(".1", ".2"), each = 5)), "max.pair.freq"))
# For case 6021 and 6081, the observed frequencies are now > 0

# Export 133 phased
write.table(L5.phased[, -c("max.pair.freq")], "phased_L5.tsv", sep = "\t", row.names = F, quote = F)

# PHASING 2 (for missing C* using NMDP reference) -----------------------------------------------------------------------------------------#

# Uses reference table "A~B~DRB1~DQB1.tsv"
# Get case IDs _without_ any missing/low-res/ambiguous haplotyping info for phasing with c("A, "B", "DRB1", "DQB1")

loci2 <- loci[loci != "C"]
coloci2 <- coloci[names(coloci) != "C"]

H.freq2 <- fread("A~B~DRB1~DQB1.tsv")
H.freq2[, (loci2) := lapply(.SD, function(x) gsub("[^0-9:]", "", sapply(strsplit(x, "*", fixed = T), `[[`, 2))), .SDcols = loci2]
setkeyv(H.freq2, loci2)

L4xC <- HLA[HLA[, Reduce(`&`, lapply(.SD, function(x) grepl("^[0-9]{2}:[0-9]{2}", x))), .SDcols = coloci2], c("ID", "Race", coloci2), with = F ]
L4xC <- L4xC[!ID %in% L5.phased$ID]
# This yields a subset of n = 100
L4xC.input <- melt(L4xC, measure.vars = patterns(paste0("^", loci2)), value.name = loci2)
L4xC.results <- lapply(split(L4xC.input, by = "ID"), function(x) phase(x, loci = loci2, ref = H.freq2))
L4xC.phased <- rbindlist(lapply(L4xC.results, function(x) as.data.table(x[1:5])))
setnames(L4xC.phased, c("ID", "Race", paste0(rep(loci2, 2), rep(c(".1", ".2"), each = 4)), "max.pair.freq"))

write.table(L4xC.phased[, -c("max.pair.freq")], "phased_L4xC.tsv", sep = "\t", row.names = F, quote = F)

# PHASING 3 (DRB1~DQB1 using NMDP reference) ---------------------------------------------------------------------------------#

loci3 <-  c("DRB1", "DQB1")
coloci3 <- coloci[names(coloci) %in% loci3]

H.freq3 <- fread("DRB1~DQB1.tsv")
H.freq3[, (loci3) := lapply(.SD, function(x) gsub("[^0-9:]", "", sapply(strsplit(x, "*", fixed = T), `[[`, 2))), .SDcols = loci3]
setkeyv(H.freq3, loci3)

# Use all for comparison
DX <- HLA[HLA[, Reduce(`&`, lapply(.SD, function(x) grepl("^[0-9]{2}:[0-9]{2}", x))), .SDcols = coloci3], c("ID", "Race", coloci3), with = F ]
# 474 with complete data
DX.input <- melt(DX, measure.vars = patterns(paste0("^", loci3)), value.name = loci3)
DX.results <- lapply(split(DX.input, by = "ID"), function(x) phase(x, loci = loci3, ref = H.freq3))
DX.phased <- rbindlist(lapply(DX.results, function(x) as.data.table(x[1:5])))
setnames(DX.phased, c("ID", "Race", paste0(rep(loci3, 2), rep(c(".1", ".2"), each = 2)), "max.pair.freq"))

# How much are results different vs phasing with known A-B-C?
DX_compare <- DX.phased[match(L5.phased$ID, ID), c("ID", coloci3), with = F]
agreement <- apply(DX_compare == L5.phased[, c("ID", coloci3), with = F], 1, all)
table(agreement) # all in agreement

DX_compare2 <-  DX.phased[match(L4xC.phased$ID, ID), c("ID", coloci3), with = F]
agreement2 <- apply(DX_compare2 == L4xC.phased[, c("ID", coloci3), with = F], 1, all)
table(agreement2)
# FALSE  TRUE
# 2    98
ids <- DX_compare2[agreement2 == F, ID]
DX.phased[ID %in% ids]
L4xC.phased[ID %in% ids]

# There is one more comparison -- see next section

# The results using more information should be more reliable
DX.phased[ID %in% ids, (coloci3) := L4xC.phased[ID %in% ids, coloci3, with = F] ]

# This is not the final version for export --
# one more reference is used with the DRB1-DQB1 data (below)

# PHASING 4 (DRB1~DQA1~DQB1 using AlleleFrequencies.net) ------------------------------------------------------------------------------#

# DQA1 typing has entries such as "05:01/0", which will be excluded
DQA1 <- unique(na.omit(grep("^[0-9]{2}:[0-9]{2}$", c(HLA$DQA1.1, HLA$DQA1.2), val = T)))

# Get haplotypes containing these DQA1 alleles from http://www.allelefrequencies.net/hla6003a.asp?
# Requires programmatic access; see http://www.allelefrequencies.net/extaccess.asp
# As of March 2020, the haplotype freq datasets don't seem to be available as flat file downloads, unfortunately.
# Use base URL hla6003a_scr.asp? to get "printer-friendly" results in one table
# instead of paginated results which requires parsing multiple pages/tables.

haps <- list()
for(a in DQA1[2:length(DQA1)]) {
  result <- xml2::read_html(paste0("http://www.allelefrequencies.net/hla6003a_scr.asp?hla_selection=DQA1*", a)) %>% rvest::html_table(fill = T)
  haps[[a]] <- result[[3]]
}
sapply(haps, nrow)
# 03:01 01:03 01:01 01:02 04:01 02:01 05:01 03:02 03:03 05:05 04:02 05:03 06:01
# 1061   731   862  1112   348   566   858   359   346   380     0    96   188

DQA1.ref <- rbindlist(haps)
DQA1.ref[, Line := NULL]
DQA1.ref <- unique(DQA1.ref)
write.table(DQA1.ref, "DQA1_ref.tsv", sep = "\t", row.names = F, quote = F) # copy of reference version

# Use only DRB1 DQA1 DQB1 given that not all entries contain HLA-I alleles and we would be more limited
# By trying to use A* B* C* as well
ref <- strsplit(DQA1.ref$Haplotype, "-")
ref <- lapply(ref, function(loci) grep("^DRB1|^DQA1|^DQB1", loci, value = T))
ref <- lapply(ref, function(x) as.list(sapply(strsplit(x, "*", fixed = T), function(xi) setNames(xi[2], xi[1]))))
ref <- rbindlist(ref, use.names = T, fill = T)
ref <- cbind(ref, DQA1.ref[, .(Population, `Frequency (%)`, `Sample Size`)])

# Check whether there are any haplotype entries without data available at these 3 loci and remove if so
loci4 <- c("DRB1", "DQA1", "DQB1")
coloci4 <- setNames(unlist(lapply(loci4, paste0, c(".1", ".2"))), rep(loci4, each = 2))
setkeyv(ref, loci4)
head(ref[ref[, Reduce(`|`, lapply(.SD, is.na)), .SDcols = loci4]])
ref <- ref[!ref[, Reduce(`|`, lapply(.SD, is.na)), .SDcols = loci4]]
ref[, `Sample Size` := as.numeric(gsub(",", "", `Sample Size`))]
ref[, Count := `Frequency (%)` * `Sample Size`]

# Because race annotation is no longer available for the populations in this reference data (see http://www.allelefrequencies.net/datasets.asp#tag_5)
# this presents a barrier to employing race-specific frequencies, which is the preferred method.
DRDQ <- HLA[HLA[, Reduce(`&`, lapply(.SD, function(x) grepl("^[0-9]{2}:[0-9]{2}", x))), .SDcols = coloci4], c("ID", "Race", coloci4), with = F ]
DRDQ.input <- melt(DRDQ, measure.vars = patterns(paste0("^", loci4)), value.name = loci4)
DRDQ.results <- lapply(split(DRDQ.input, by = "ID"), phase, loci = loci4, ref = ref, withRace = F, percentFreq = F, freqCol = "Count")
DRDQ.phased <- rbindlist(lapply(DRDQ.results, function(x) as.data.table(x[1:5])))
setnames(DRDQ.phased, c("ID", "Race", paste0(rep(loci4, 2), rep(c(".1", ".2"), each = 3)), "max.pair.freq"))

# Another comparison with previously phased DRB1~DQB1
DX_compare3 <-  DX.phased[match(DRDQ.phased$ID, ID), c("ID", coloci3), with = F]
agreement3 <- apply(DX_compare3 == DRDQ.phased[, c("ID", coloci3), with = F], 1, all)
table(agreement3)
# FALSE  TRUE
# 9   465
ids <- DX_compare3[agreement3 == F, ID]
DX.phased[ID %in% ids, ] # vs DRDQ.phased[ID %in% ids, -c("DQA1.1", "DQA1.2"), with = F]
# View discrepant results for phasing -- some are actually not different, just inverted
agreement4 <- apply(DX.phased[ID %in% ids, c("DRB1.1", "DQB1.1", "DRB1.2", "DQB1.2")] ==
                    DRDQ.phased[ID %in% ids, c("DRB1.2", "DQB1.2", "DRB1.1", "DQB1.1"), with = F],
                    1, all)
table(agreement4)
# FALSE  TRUE
# 5     4

ids <- DX.phased[ID %in% ids][agreement4 == F, ID]
DX.phased[ID %in% ids] # vs DRDQ.phased[ID %in% ids]
# Interestingly, 4 out of 5 discrepant cases are African Americans
AFA.cases <- DRDQ.phased[ID %in% ids][Race == "African Am", ID]
# We can attept to get more race-specific frequencies with manual annotation of the
# populations present -> relevant ancestral populations for African Americans:
unique(ref$Population) # view populations
AFAref <- c("Jordan Amman", "Kenya, Nyanza Province, Luo tribe", "South Africa Worcester",
         "Congo Kinshasa Bantu", "Equatorial Guinea Bioko Island Bubi", "Morocco",
         "Cameroon Yaounde", "Gabon Haut-Ogooue Dienga", "Morocco Souss Region",
         "Ethiopia Amhara", "Ethiopia Oromo", "Morocco Atlantic Coast Chaouya",
         "Algeria pop 2", "Morocco Settat Chaouya")
AFAref <- ref[Population %in% AFAref]
AFA.results <- lapply(split(DRDQ.input[ID %in% AFA.cases], by = "ID"), phase, loci = loci4, ref = AFAref, withRace = F, percentFreq = F, freqCol = "Count")
AFA.phased <- rbindlist(lapply(AFA.results , function(x) as.data.table(x[1:5])))
setnames(AFA.phased, c("ID", "Race", paste0(rep(loci4, 2), rep(c(".1", ".2"), each = 3)), "max.pair.freq"))
AFA.phased # vs DX.phased[ID %in% AFA.cases]
# For case 6007 the results are now the same
DRDQ.phased[ID == 6007, ] <- AFA.phased[ID == 6007, ]

write.table(DRDQ.phased[, -c("max.pair.freq")], file = "phased_DRB1_DQA1_DQB1.tsv", sep = "\t", row.names = F, quote = F)

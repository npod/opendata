library(rvest)
library(dplyr)
library(tidyr)

tabs <- read_html("https://academic.oup.com/jcem/article/103/12/4343/5091458") %>% html_table(fill = T)
tab2 <- tabs[[8]]
tab2 <- tab2[3:21, c(1, 3:ncol(tab2))]
names(tab2) <- c("ID", unlist(lapply(c("flu", "CXCL10"), paste, c("head", "body", "tail", "duod"), sep =  ".")))

# Score: 0, none; 1, ≤20 cells per field; 2, 20–40 cells per field; 3, >40 cells per field.
# Some values have a ? denoting uncertainty
tab2 <- tab2 %>%
  filter(grepl("^6", ID)) %>%
  mutate_all(function(x) as.numeric(gsub("[^0-9]", "", x)))

# Because there are many NAs and some uncertain values,
# we don't try to generate a whole-pancreas level summary value
# by combining results from all head, body, and tail samples,
# though this has been done for other data.

write.table(tab2, "PMID30203067_1.tsv", sep = "\t", row.names = F, quote = F)

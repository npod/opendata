# Publication: https://pubmed.ncbi.nlm.nih.gov/29671030-abnormal-islet-sphingolipid-metabolism-in-type-1-diabetes/
# (on Pubmed page) Data availability: The RNA expression data is available online at
# https://www.dropbox.com/s/93mk5tzl5fdyo6b/Abnormal%20islet%20sphingolipid%20metabolism%20in%20type%201%20diabetes%2C%20RNA%20expression.xlsx?dl=0

# Bioconductor packages version 3.10
library(org.Hs.eg.db)

dat <- read.table("PMID29671030_Holm18.tsv", header = T)
symbols <- colnames(dat)
entrez <- mapIds(org.Hs.eg.db, symbols, "ENTREZID", "SYMBOL")

# Some names/symbols without ID mappings because of poor notation
entrez[is.na(entrez)]

# Manual human gene lookup results at https://www.ncbi.nlm.nih.gov/gene/
manual <-
c(ssSPTa = 171546,
  ssSPTb = 165679,
  CerS2 = 29956,
  CerS3 = 204219,
  CerS4 = 79603,
  CerS5 = 91012,
  CerS6 = 253782,
  CGT = 7368, # alias
  CST = 9514, # alias
  Prosaposin = 5660, # should be PSAP
  FAPP2 = 84725, # alias
  CERT = 10087 # should be CERT1
)

# A number of names appear to be typos because according to manual verification:
# VSP51 (does not exist) -> should be VPS51; VSP52 -> VPS52; etc.
typos <-
c(KDRS = 2531, # KDSR
  VSP51 = 738, # VPS51
  VSP52 = 6293,
  VSP53 = 55275,
  VSP54 = 51542)

entrez[names(manual)] <- manual
entrez[names(typos)] <- typos

# Back to approved symbols
symbols <- mapIds(org.Hs.eg.db, entrez, "SYMBOL", "ENTREZID")
colnames(dat) <- symbols
dat <- cbind(ID = as.numeric(rownames(dat)), dat)

# Filter out DIVID data
dat <- dat[!is.na(dat$ID), ]

# Export dataset
write.table(dat, "PMID29671030_1.tsv", sep = "\t", row.names = F, quote = F)

# Export gene ID mapping
writeLines(entrez, "PMID29671030_1_GeneID.txt", sep = "\n")

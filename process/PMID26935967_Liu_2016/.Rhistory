library(GEOquery)
library(org.Hs.eg.db)
library(mygene)
gse <- "GSE72492" # Submitted 2015
gse <- getGEO(gse)
eset <- gse[[1]]
meta <- pData(eset)
ids <- as.numeric(meta$`npod id:ch1`)
annotation(eset)
# "GPL14550" SurePrint G3 Human GE 8x60K Microarray last updated Oct 11, 2016 by GEO
# It appears that annotation file was updated after submission,
# so probe-to-gene assignments can change and (re)annotation of this dataset
# might yield some differences
gpl <- getGEO(annotation(eset))
gpl <- Table(gpl)
colnames(gpl) # GPL files can vary somewhat, so check column names
probes <- row.names(eset)
# With expression matrix, filter out features lacking entrez gene annotation
entrez <- gpl$GENE[match(probes, gpl$ID)]
head(entrez)
table(is.na(entrez))
xm <- exprs(eset)
xm <- xm[!is.na(entrez), ]
entrez <- entrez[!is.na(entrez)]
# Look up current symbols with entrez IDs
gene <- mapIds(org.Hs.eg.db, as.character(entrez), "SYMBOL", "ENTREZID")
table(is.na(gene)) # 975
need_update <- names(gene)[is.na(gene)]
query <- mygene::getGenes(unique(need_update), fields = "symbol", return.as = "DataFrame")
table(is.na(query$notfound)) # 182
head(query)
updated <- query$symbol[match(need_update, query$query)]
gene[is.na(gene)] <- updated
table(is.na(gene))
xm <- xm[!is.na(gene), ]
gene <- gene[!is.na(gene)]
head(gene)
whichMaxVar <- function(genes, xm) {
IQRs <- apply(xm, 1, IQR, na.rm = T)
dupx <- table(genes)
dupx2 <- names(dupx)[dupx > 1] # genes with more than one reporter
dupx2 <- sapply(dupx2,
function(x) {
i <- which(genes %in% x) # indices of all probes for gene
i <- i[which.max(IQRs[i])] # index with highest var
return(i)
}
)
keep <- vector("integer", length(dupx))
keep[dupx == 1] <- match(names(dupx)[dupx == 1], genes)
keep[dupx > 1] <- dupx2
names(keep) <- names(dupx)
return(keep)
}
keep <- whichMaxVar(gene, xm)
head(keep)
head(genes)
head(gene)
dupx <- table(genes)
dupx2 <- names(dupx)[dupx > 1] # genes with more than one reporter
dupx <- table(gene)
head(dupx)
keep <- whichMaxVar(gene, xm)
head(keep)
head(gene)
setwd("~/nPOD/inst/data-raw/process")
setwd("~/nPOD/inst/data-raw/process/PMID26935967_Liu_2016")
# Bioconductor packages version 3.10
library(org.Hs.eg.db)
library(data.table)
library(httr)
px <- fread("Liu-2016_TableS1.tsv", skip = 1L)
px <- px[, c("Majority protein IDs", "Protein names", "Gene names",
"Sequence coverage [%]", "Peptides", "Unique peptides", "Score", "Q-value",
grep("^HC|^T1D", names(px), val = T)), with = F][-.N] # remove last line footnote
# Mappings to case IDs rely on the order in https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4893790/table/T1/
setnames(px, c("Uniprot", "Protein", "Gene", "SeqCov", "Peptides", "Unique", "Score", "Qval",
as.character(c(6029, 6057, 6096, 6172, 6174, 6195, 6196, 6051, 6211, 6212))))
# The original gene annotation is retained as long as it can be verified as the official symbol
# For genes where the annotation is missing or doesn't appear to be to the official symbol,
# re-annotation is done using the given Uniprot ACC/ID.
genes <- strsplit(px$Gene, ";")
gene1 <- sapply(genes, function(x) if(length(x)) x[1] else NA)
table(is.na(gene1)) # 31 where no gene annotation present
# Get entrez ID using the first gene annotation to test that these are current canonical symbols
entrezmap <- mapIds(org.Hs.eg.db, na.omit(gene1), "ENTREZID", "SYMBOL")
table(is.na(entrezmap)) # 247, many of the no-match are aliases
# 31 + 247; use given Uniprot accessions for these
# https://www.uniprot.org/help/api_idmapping
uniprots <- strsplit(px$Uniprot, ";")
uniprot1 <- sapply(uniprots, function(x) x[1])
ind <- c(which(is.na(gene1)), which(gene1 %in% names(entrezmap)[is.na(entrezmap)]))
uniprot1NA <- uniprot1[ind]
umap <- httr::GET("https://www.uniprot.org/uploadlists/",
# Note: Uniprot also maps to EntrezID with P_ENTREZGENEID
query = list(from = "ACC+ID", to = "GENENAME",
format = "tab", query = paste0(uniprot1NA, collapse = " ")))
umap <- read.table(text = content(umap, as = "text", encoding = "UTF-8"), header = T, sep = "\t", stringsAsFactors = F)
# Uniprot table contains only entries that were successfully mapped (243)
uniprot1NA[!uniprot1NA %in% umap$From] # 52 entries not returned, majority are isoforms
isoquery <- unique(regmatches(uniprot1NA[!uniprot1NA %in% umap$From],
regexpr(".*(?=-)", uniprot1NA[!uniprot1NA %in% umap$From], perl = T)))
iso <- httr::GET("https://www.uniprot.org/uploadlists/",
query = list(from = "ACC+ID", to = "GENENAME",
format = "tab", query = paste0(isoquery, collapse = " ")))
iso <- read.table(text = content(iso, as = "text", encoding = "UTF-8"), header = T, sep = "\t", stringsAsFactors = F)
iso$From <- unique(grep("-", uniprot1NA[!uniprot1NA %in% umap$From], invert = F, value = T))
# All 45 isoforms can be mapped to a gene, while the rest need to be looked at more manually
# Mappings below use the last pre-obsolete version for currently osolete Uniprot accs
grep("-", uniprot1NA[!uniprot1NA %in% umap$From], invert = T, value = T)
# [1] "A0A087WW55" "A0A087WYC5" "H3BNX3"     "I3L4J1"     "A0A087WUL0" "A0A096LPI6" "E9PJB8"
manual <- data.frame(From = unique(grep("-", uniprot1NA[!uniprot1NA %in% umap$From], invert = T, value = T)),
To = c("TUBGCP3", # https://www.uniprot.org/uniprot/A0A087WWB5.txt?version=17
"IGHG1", # https://www.uniprot.org/uniprot/A0A087WYC5.txt?version=17
NA, # https://www.uniprot.org/uniprot/H3BNX3 (status predicted protein)
NA, # https://www.uniprot.org/uniprot/I3L4J1
"TKFC", # https://www.uniprot.org/uniprot/A0A087WUL0.txt?version=16
NA, # https://www.uniprot.org/uniprot/A0A096LPI6
"VIMP" # https://www.uniprot.org/uniprot/E9PJB8.txt?version=27
))
uniprot1map <- do.call(rbind, list(umap, iso, manual))
uniprot1map <- setNames(uniprot1map$To, uniprot1map$From)[uniprot1NA]
symbol <- gene1
symbol[ind] <- uniprot1map
# (For additional review) table below represents entries where re-annotation resulted in changes
changes <- data.table(px$Uniprot, px$Gene, symbol)[ind,  ]
px.t <- t(px[, grepl("^6", names(px)), with = F])
px.t <- cbind(as.numeric(rownames(px.t)), px.t)
colnames(px.t) <- c("ID", uniprot1)
entrez <- mapIds(org.Hs.eg.db, symbol, "ENTREZID", "SYMBOL")
need_update <- which(is.na(entrez))
need_update
head(entrez)
entrez$SELENOS
entrez[["SELENOS"]]
# (For additional review) table below represents entries where re-annotation resulted in changes
changes <- data.table(px$Uniprot, px$Gene, symbol)[ind,  ]
changes
entrez
need_update <- which(is.na(entrez))
need_update
## version 1
# update <- c(DIABLO = 56616, GET3 = 439, `hCG_2004001` = NA, SELENOS = 55829, C11orf58 = 10944, #
#            `MT-CO2` = 4513, `MT-CO3` = 4514, `MT-ND4` = 4538, `MT-ND4` = 4540, H2AZ2 = 94239,
#            H2AZ2 = 94239, WDR92 = 116143, `H1-10` = 8971, C1orf116 = 79098, MACROH2A2 = 55506)
## version 2
update <- c(DIABLO = 56616, GET3 = 439, `hCG_2004001` = NA, SELENOS = 55829, C11orf58 = 10944,
`MT-CO2` = 4513, `MT-CO3` = 4514, `MT-ND4` = 4538, `MT-ND4` = 4540, WDR92 = 116143,
C1orf116 = 79098)
entrez[need_update] <- update
table(is.na(entrez))
head(entrez)
unlist(a = NA, b = 1)
unlist(list(a = NA, b = 1))
writeLines(unlist(entrez), "PMID26935967_1_GeneID.txt", sep = "\n")
length(unlist(entrez))
test <- vector(entrez)
writeLines(c(NA, 1, 3), "test.txt")
writeLines(c(NA, "1", "3"), "test.txt")
length(entrez)
ids <- sapply(entrez, `[[`, 1)
head(ids)
ids <- sapply(entrez, `[`, 1)
head(ids)
entrez[["WDR92"]]
head(entrez)
which(sapply(entrez, is.null))
entrez[[211]]
entrez[211]
head(symbol)
table(is.na(symbol))
entrez$<<NA>
entrez[210:212]
table(is.na(names(entrez)))
entrez[which(sapply(entrez, is.null))] <- NA
ids <- unlist(entrez)
head(ids)
names(ids)[need_update] <- names(update)
ids[need_update] <- update
head(ids)
writeLines(ids, "PMID26935967_1_GeneID.txt", sep = "\n")
writeLines(names(ids), "PMID26935967_1_GeneHGNC.txt", sep = "\n")

library(readxl)
library(tidyr)
library(dplyr)

CD68 <- read_xlsx("all_data_final.xlsx", sheet = "CD68")
CD68 <- CD68 %>%
  group_by(Case) %>%
  summarize(hosp.bin = first(hospbin),
            CD68.prct = mean(`% CD68 Positive Cells`),
            CD68.prct_SD = sd(`% CD68 Positive Cells`),
            Ins.prct = mean(`% Insulin Positive Cells`),
            Ins.prct_SD = sd(`% Insulin Positive Cells`))

Ki67 <- read_xlsx("all_data_final.xlsx", sheet = "Ki67")
Ki67 <- Ki67 %>%
  group_by(Case) %>%
  summarize(Ki67.prct = mean(`% Ki67 Positive Cells`),
            Ki67.prct_SD = sd(`% Ki67 Positive Cells`),
            Ki67.Ins.prct = mean(`% Dual Positive Cells`),
            Ki67.Ins.prct_SD = sd(`% Dual Positive Cells`))

CD45 <- read_xlsx("all_data_final.xlsx", sheet = "CD45")
CD45 <- CD45 %>%
  group_by(Case) %>%
  summarize(CD45.prct = mean(`% CD45 Positive Cells`),
            CD45.prct_SD = sd(`% CD45 Positive Cells`))

dataset <- full_join(CD68, Ki67) %>% full_join(CD45) %>%
  rename(ID = Case)

write.table(dataset, "PMID29128936_1.tsv", sep = "\t", quote = F, row.names = F)

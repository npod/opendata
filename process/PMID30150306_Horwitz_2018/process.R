library(readxl)
library(tidyr)
library(dplyr)
library(Hmisc)

# The two sheets contain much the same data; in the first the data are summarized for each donor and grouped by type,
# while the second sheet contains pre-summarized slide values for a very small subset of (3) donors
# where infiltrate grade was also assessed.
data1 <- read_xlsx("Horwitz_et_al_-_53BP1_results_and_infiltrate_grades.xlsx", sheet = "53BP1 by type", range = "D4:I52")
data1 <- data1 %>%
  select(ID = `Slide ID`,
         a53BP1.prct = `percent activated 53BP1 cells`,
         images.n = `images counted`) %>%
  mutate(ID = gsub("-.*", "", ID))

# Though the second sheet contains data for only 3 donors, it seems to be high-quality data that should be included
data2 <- read_xlsx("Horwitz_et_al_-_53BP1_results_and_infiltrate_grades.xlsx", sheet = "53BP1 by infiltrate", range = "F7:K150")
data2 <- data2 %>%
  select(ID = `Slide ID`,
         images.n = `images counted`,
         a53BP1 = `activated 53BP1 cells`,
         cells.n = `total cells`,
         a53BP1.prct = `percent activated 53BP1 cells`,
         islet.infiltrate.grade = `islet infiltrate grade`) %>%
  mutate(ID = gsub("-.*", "", ID)) %>%
  group_by(ID)

# Can see that the number of images sampled vary quite a bit, e.g. from 1-54,
# so islet infiltrate grade should be weighted
data2 %>% summarize(range = paste(range(images.n), collapse = "-"))

data2 <- data2 %>%
  summarize(a53BP1.prct = round(sum(a53BP1)/sum(cells.n) * 100, 3),
            islet.infiltrate.grade_SD = round(sqrt(Hmisc::wtd.var(islet.infiltrate.grade, w = images.n)), 3),
            islet.infiltrate.grade = round(weighted.mean(islet.infiltrate.grade, w = images.n), 3),
            n = sum(images.n))

# The percentages in data2 should be somewhat better because of much larger images used (the percentages are nearly the same)
data1 %>% filter(ID %in% data2$ID)

# append infiltrate.grade data

dataset <- full_join(data1[, 1:2], data2[, c(1, 4, 3)], by = "ID") %>%
  mutate(a53BP1.prct = round(a53BP1.prct, 3))

write.table(dataset, "PMID30150306_1.tsv", sep = "\t", quote = F, row.names = F)

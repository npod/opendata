library(readxl)
library(tidyr)
library(dplyr)
library(readxl)

dat <- read_excel("nPOD nanotomy immune cell scoring table.xlsx", range = "M3:AJ70")

# Mast Cell (MC) Total, Tryptase+, Macrophages and Neutrophils are presented as count/(area or volume)
# Use /10^5 μm2 (area) value consistently for all

# Check columns
names(dat)
dat <- dat %>%
  select(ID = `...1`,
         mastcell.density = `/10^5 μm2...13`,
         tryptase.density = `/10^5 μm2...17`,
         macrophage.density = `/10^5 μm2...20`,
         neutrophil.density = `/10^5 μm2...24`)
head(dat)  

# Remove empty rows
dat2 <- dat %>% filter(!is.na(ID))

# Separate case IDs and sample IDs
# This will be the un-summarized version
dat2 <- dat2 %>%
  mutate(sampleID = paste(ID, "32424134", sep = "-"),
         ID = as.integer(gsub("[a-z]", "", ID))) %>%
  relocate(sampleID, .after = ID)

# Area per mm is more standard, so conversion
# from 10^5 micrometer^2 -> mm^2
dat2 <- dat2 %>%
  mutate_at(c("mastcell.density", "tryptase.density", "macrophage.density", "neutrophil.density"), function(x) x * 10)

# Summary version
dat3 <- dat2 %>%
  group_by(ID) %>%
  summarize(mastcell.density = mean(mastcell.density),
            tryptase.density = mean(tryptase.density),
            macrophage.density = mean(macrophage.density),
            neutrophil.density = mean(neutrophil.density))

# Export both versions
write.table(dat2, "PMID32424134_1L.tsv", sep = "\t", quote = F, row.names = F)
write.table(dat3, "PMID32424134_1.tsv", sep = "\t", quote = F, row.names = F)



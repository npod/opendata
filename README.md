## nPOD OpenData Repository

This repository contains:
- In `data`, data that has been contributed by authors or curated from papers and converted into a more standardized, **interoperable** `.tsv` format (e.g. from PDFs, Excel, and sometimes images).
- In `metadata`, metadata at dataset and feature level, consisting of manual annotations using ontology-terms or controlled vocabularies in open RDF format. 
- In `process`, scripts and documentation for any extract/transform/re-annotation (i.e. to check and update gene/protein ID mappings for some expression datasets) operations done programmatically. Not every dataset has associated scripts, e.g. datasets that were translated manually or using conversion tools from PDF.

## Contributions and Corrections

If you'd like to add data/metadata or make a correction, create a new issue.

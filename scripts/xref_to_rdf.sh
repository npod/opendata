#!/bin/bash

# This should be run at the root of the repo.
# This creates RDF from cross-reference tables in metadata/xref and is run as part of the release pipeline
# (similar to maintaining mappings between ENSEMBL and NCBIGene ids) 
# Currently, we only maintain mappings between NCBI BioSample ids and nPOD Case ids
# -- See "nPOD Launched RRIDs for All Biosamples!" - March 1, 2021 (https://dknet.org/about/blog/2226)
# Though ID mapping between dbs is most common, in the future there might be other types of cross-references 
# where mapping requires different semantics and should not default to using owl:sameAs

OUTPUT_DIR="metadata/xref"


while getopts ":o:" option; do
   case $option in
      o) # Output directory
         OUTPUT_DIR=$OPTARG;;
     \?) # Invalid option
         echo "Error: Invalid option"
         exit;;
   esac
done


echo "@prefix owl: <http://www.w3.org/2002/07/owl#> ." > ${OUTPUT_DIR}/NCBI-BioSample.ttl
awk -F',' 'NR!=1 { gsub("RRID:","<https://www.ncbi.nlm.nih.gov/biosample/",$1); gsub("\r",">",$2); print $1 "> owl:sameAs <https://npoddatashare.coh.org/labkey/study/nPOD%20DataShare/participant.view?participantId=" $2 }' metadata/xref/NCBI-BioSample.csv >> ${OUTPUT_DIR}/NCBI-BioSample.ttl




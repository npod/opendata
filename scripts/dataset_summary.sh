#!/bin/bash

# This should be run at the root of the repo.
# This outputs a file with 5 lines of relevant summary stats:
# 1) number of total datasets
# 2) sum of features
# 3) sum of dadata points
# 4) unique assays
# 5) unique cell tissue classes

# Sum number of dataset records, 
# numFeatures (column 17) and 
# dataPoints (column 18) in metadata/dataset/master.csv
awk -F'\t' '{ sumfeats += $17; sumpoints += $18 } END { print NR, sumfeats, sumpoints }' metadata/dataset/master.csv | tr ' ' '\n' > dataset_summary

# Assay type (column 8)
awk -F'\t' '{ if (NR > 2) print $8 }' metadata/dataset/master.csv | tr '|' '\n' | sort | uniq -c | grep 'http://purl.obolibrary.org/' | wc -l >> dataset_summary

# Calls other summary script to produce unique_cell_tissue_classes file
./scripts/unique_cell_tissue_class.sh
wc -l unique_cell_tissue_classes >> dataset_summary

#!/bin/bash

# This should be run at the root of the repo.
# Check that dataset features have synced annotations, 
# i.e. variables in head of dataset table have corresponding row in feature/*.csv
# Only failed (flagged) results are written to check_results/check1/*_failed.txt

FAIL=0
mkdir check_results/check1
mkdir tmp
for dataset in data/main/*; do
  id=${dataset##*/} 
  meta=metadata/feature/${id%tsv}csv
  head -n1 $dataset | sed 's/\t/\n/g' > tmp/${id%tsv}txt
  awk -F',' 'NR>1{print $3}' $meta > tmp/${id%.tsv}_meta.txt 
  paste tmp/${id%tsv}txt tmp/${id%.tsv}_meta.txt | awk '$1 != $2 { print $1}' > check_results/check1/${id%.tsv}_result.txt
  flag=$(wc -l < check_results/check1/${id%.tsv}_result.txt)
  let FAIL+=$flag
  if (($flag))
  then 
    mv check_results/check1/${id%.tsv}_result.txt check_results/check1/${id%.tsv}_failed.txt
  else 
    rm check_results/check1/${id%.tsv}_result.txt
  fi
done
if(($FAIL)) 
then 
  echo "$FAIL files failed sync check, see check_results/check1 artifacts."
  exit 1
else
  echo "Passed sync check!"
fi

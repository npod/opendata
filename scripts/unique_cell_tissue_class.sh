#!/bin/bash

# Compose unique list of cell tissue (column 13) from feature metadata
touch cell_tissue_classes
for meta in metadata/feature/*; do
  gawk -v FPAT='[^,]*|("[^"]*")+' '{ if (NR>1) print $13}' $meta >> cell_tissue_classes
done
sort cell_tissue_classes | uniq -c | grep obo | sort -nr > unique_cell_tissue_classes
rm cell_tissue_classes